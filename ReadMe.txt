オープンアプリ『ＯＡＰ通信料概算』

その日オープンアプリで行ったパケット通信料金を概算します


アプリ配布リンク: http://tomarky.html.xdomain.jp/mobile/oap/p027.hdml


※オープンアプリとはau携帯電話(ガラケー)で動かすことができるアプリのことです
　(参考リンク http://www.au.kddi.com/ezfactory/tec/spec/openappli.html ）