// @@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@ //
// MyRecord.java  レコードストア処理
//
// (C)Tomarky   2009.01.01-
// @@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@ //
//package MyPackage;

//インポート
//import javax.microedition.lcdui.*;
//import javax.microedition.lcdui.game.*;
//import javax.microedition.midlet.*;
//import java.util.*;
import javax.microedition.rms.*;
import java.io.*;

// @@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@ //

//
public final class MyRecord {

// @@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@ //
// @@@@ 基本 byte型配列 @@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@ //
// @@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@ //

	public static final boolean saveBytes(String recordname, byte[] bData) {
		RecordEnumeration     re = null;
		RecordStore           rs = null;
		
		try {
			//レコードストアの接続
			rs=RecordStore.openRecordStore(recordname,true);
			
			if (rs.getNumRecords()==0) {
				//レコードの追加
				rs.addRecord(bData,0,bData.length);
			} else {
				//レコードの更新
				re=rs.enumerateRecords(null,null,false);
				int recordId=re.nextRecordId();
				rs.setRecord(recordId,bData,0,bData.length);
				re.destroy(); re=null;
			}

			//レコードストアの切断
			rs.closeRecordStore(); rs=null;
		} catch (Exception e) {
			System.out.println("MyRecord.saveBytes::"+e.toString());
			if (re!=null) {
				try { re.destroy(); re=null; } catch (Exception e2) {}
			}
			if (rs!=null) {
				try { rs.closeRecordStore(); rs=null; } catch (Exception e2) {}
			}
			return false;
		}
		return true;
	}

// @@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@ 基本 byte型配列 @@@ //

	public static final byte[] loadBytes(String recordname) {
		RecordEnumeration     re    = null;
		RecordStore           rs    = null;
		byte[]                bData = null;
		
		try {
			int recordId=1;
			
			//レコードストアの接続
			rs=RecordStore.openRecordStore(recordname,false);
			
			if (rs.getNumRecords()==0) { //レコードがない！
				rs.closeRecordStore(); rs=null;
				return null;

			} else {
				//レコードの読み出し
				re=rs.enumerateRecords(null,null,false);
				recordId=re.nextRecordId();
				re.destroy(); re=null;
			}
			
			bData=rs.getRecord(recordId);

			//レコードストアの切断
			rs.closeRecordStore(); rs=null;

		} catch (Exception e) {
			System.out.println("MyRecord.loadBytes::"+e.toString());
			if (re!=null) {
				try { re.destroy(); re=null; } catch (Exception e2) {}
			}
			if (rs!=null) {
				try { rs.closeRecordStore(); rs=null; } catch (Exception e2) {}
			}
			return null;
		}
		
		return bData;
	}

// @@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@ //
// @@@@@ long型配列 @@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@ //
// @@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@ //
/*
	//データ(long型配列)の書き込み
	public static final boolean saveLongs(String recordname, long[] value) {  //-----★
		ByteArrayOutputStream out   = null;
		DataOutputStream      dos   = null;
		byte[]                bData = null;

		try {
		
			//バイトデータに変換
			int n=value.length;
			out=new ByteArrayOutputStream(1);
			dos=new DataOutputStream(out);
			dos.writeInt(n);
			for (int i=0;i<n;i++)
				dos.writeLong(value[i]);  //--------★
			bData=out.toByteArray();
			dos.close(); dos=null; out=null;

		} catch (Exception e) {
			System.out.println("MyRecord.saveLongs::"+e.toString());  //-----★
			if (dos!=null) {
				try { dos.close(); dos=null; out=null; } catch (Exception e2) {}
			}
			if (out!=null) {
				try { out.close(); out=null; } catch (Exception e2) {}
			}
			return false;
		}
		return saveBytes(recordname,bData);
	}

// @@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@ long型配列 @@@ //

	//データ(long型配列)の読み込み
	public static final long[] loadLongs(String recordname) {  //-----★
	
		ByteArrayInputStream  in    = null;
		DataInputStream       dis   = null;
		long[]                value = null;  //-----★
		
		byte[] bData=loadBytes(recordname);
		if (bData==null) return null;
		
		try {
			//データの読み出し
			in=new ByteArrayInputStream(bData);
			dis=new DataInputStream(in);
			
			int n=dis.readInt();
			value=new long[n];  //---------------★
			for (int i=0;i<n;i++) {
				value[i]=dis.readLong();  //-----★
			}
			
			dis.close(); dis=null; in=null;

		} catch (Exception e) {
			System.out.println("MyRecord.loadLongs::"+e.toString());  //-----★
			if (dis!=null) {
				try { dis.close(); dis=null; in=null; } catch (Exception e2) {}
			}
			if (in!=null) {
				try { in.close(); in=null; } catch (Exception e2) {}
			}
			return null;
		}
		
		return value;
	}
//*/

// @@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@ //
// @@@@@ int型配列 @@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@ //
// @@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@ //
/*
	//データ(int型配列)の書き込み
	public static final boolean saveInts(String recordname, int[] value) {  //-----★
		ByteArrayOutputStream out=null;
		DataOutputStream      dos=null;
		byte[]                bData=null;

		try {
		
			//バイトデータに変換
			int n=value.length;
			out=new ByteArrayOutputStream(1);
			dos=new DataOutputStream(out);
			dos.writeInt(n);
			for (int i=0;i<n;i++)
				dos.writeInt(value[i]);  //------------★
			bData=out.toByteArray();
			dos.close(); dos=null; out=null;

		} catch (Exception e) {
			System.out.println("MyRecord.saveInts::"+e.toString());  //-----★
			if (dos!=null) {
				try { dos.close(); dos=null; out=null; } catch (Exception e2) {}
			}
			if (out!=null) {
				try { out.close(); out=null; } catch (Exception e2) {}
			}
			return false;
		}
		return saveBytes(recordname,bData);
	}

// @@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@ int型配列 @@@ //

	//データ(int型配列)の読み込み
	public static final int[] loadInts(String recordname) {  //-----★
	
		ByteArrayInputStream  in=null;
		DataInputStream       dis=null;
		RecordEnumeration     re=null;
		RecordStore           rs=null;
		int[]                 value=null;  //-----★
		
		byte[] bData=loadBytes(recordname);
		if (bData==null) return null;
		
		try {
			//データの読み出し
			in=new ByteArrayInputStream(bData);
			dis=new DataInputStream(in);
			
			int n=dis.readInt();
			value=new int[n];  //---------------★
			for (int i=0;i<n;i++) {
				value[i]=dis.readInt();  //-----★
			}
			
			dis.close(); dis=null; in=null;

		} catch (Exception e) {
			System.out.println("MyRecord.loadInts::"+e.toString());  //-----★
			if (dis!=null) {
				try { dis.close(); dis=null; in=null; } catch (Exception e2) {}
			}
			if (in!=null) {
				try { in.close(); in=null; } catch (Exception e2) {}
			}
			return null;
		}
		
		return value;
	}
//*/

// @@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@ //
// @@@@@ int型 @@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@ //
// @@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@ //
/*
	//データ(int型)の書き込み
	public static final boolean saveInt(String recordname, int value) {  //-----★
		
		ByteArrayOutputStream out=null;
		DataOutputStream      dos=null;
		byte[]                bData=null;
		
		try {
			//バイトデータに変換
			out=new ByteArrayOutputStream(1);
			dos=new DataOutputStream(out);
			dos.writeInt(value);  //-----------------------★
			bData=out.toByteArray();
			dos.close(); dos=null; out=null;
		
		} catch (Exception e) {
			System.out.println("MyRecord.saveInt::"+e.toString());  //-----★
			//例外処理
			if (dos!=null) {
				try { dos.close(); dos=null; out=null; } catch (Exception e2) {}
			}
			if (out!=null) {
				try { out.close(); out=null; } catch (Exception e2) {}
			}
			return false;
		}
		return saveBytes(recordname,bData);
	}

// @@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@ int型 @@@ //

	//データ(int型)の読み込み
	public static final int loadInt(String recordname, int defaultValue) {  //-----★

		ByteArrayInputStream  in=null;
		DataInputStream       dis=null;
		int                   value=defaultValue;  //-----★
		
		byte[] bData=loadBytes(recordname);
		if (bData==null) return defaultValue;

		try {
			//データの読み出し
			in=new ByteArrayInputStream(bData);
			dis=new DataInputStream(in);
			
			value=dis.readInt();  //-----★
			
			dis.close();

		} catch (Exception e) {
			System.out.println("MyRecord.loadInt::"+e.toString());  //-----★
			if (dis!=null) {
				try { dis.close(); dis=null; in=null; } catch (Exception e2) {}
			}
			if (in!=null) {
				try { in.close(); in=null; } catch (Exception e2) {}
			}
			return defaultValue;
		}
		return value;
	}
//*/

// @@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@ //
// @@@@@ long型 @@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@ //
// @@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@ //
/*
	//データ(long型)の書き込み
	public static final boolean saveLong(String recordname, long value) {  //-----★
		
		ByteArrayOutputStream out=null;
		DataOutputStream      dos=null;
		byte[]                bData=null;
		
		try {
			//バイトデータに変換
			out=new ByteArrayOutputStream(1);
			dos=new DataOutputStream(out);
			dos.writeLong(value);  //-------------------★
			bData=out.toByteArray();
			dos.close(); dos=null; out=null;
			
		} catch (Exception e) {
			System.out.println("MyRecord.saveLong::"+e.toString());  //-----★
			//例外処理
			if (dos!=null) {
				try { dos.close(); dos=null; out=null; } catch (Exception e2) {}
			}
			if (out!=null) {
				try { out.close(); out=null; } catch (Exception e2) {}
			}
			return false;
		}
		return saveBytes(recordname,bData);
	}

// @@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@ long型 @@@ //

	//データ(long型)の読み込み
	public static final long loadLong(String recordname, long defaultValue) {  //-----★

		ByteArrayInputStream  in=null;
		DataInputStream       dis=null;
		long                  value=defaultValue;  //-----★
		
		byte[] bData=loadBytes(recordname);
		if (bData==null) return defaultValue;

		try {
			//データの読み出し
			in=new ByteArrayInputStream(bData);
			dis=new DataInputStream(in);
			
			value=dis.readLong();  //-----★
			
			dis.close();

		} catch (Exception e) {
			System.out.println("MyRecord.loadLong::"+e.toString());  //-----★
			if (dis!=null) {
				try { dis.close(); dis=null; in=null; } catch (Exception e2) {}
			}
			if (in!=null) {
				try { in.close(); in=null; } catch (Exception e2) {}
			}
			return defaultValue;
		}
		return value;
	}
//*/

// @@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@ //
// @@@@@ double型 @@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@ //
// @@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@ //
//*
	//データ(double型)の書き込み
	public static final boolean saveDouble(String recordname, double value) {  //-----★
		
		ByteArrayOutputStream out=null;
		DataOutputStream      dos=null;
		
		byte[]                bData=null;
		
		try {
			//バイトデータに変換
			out=new ByteArrayOutputStream(1);
			dos=new DataOutputStream(out);
			dos.writeDouble(value); //--------------★
			bData=out.toByteArray();
			dos.close(); dos=null; out=null;
			
		} catch (Exception e) {
			System.out.println("MyRecord.saveDouble::"+e.toString());  //-----★
			//例外処理
			if (dos!=null) {
				try { dos.close(); dos=null; out=null; } catch (Exception e2) {}
			}
			if (out!=null) {
				try { out.close(); out=null; } catch (Exception e2) {}
			}
			return false;
		}
		return saveBytes(recordname,bData);
	}

// @@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@ double型 @@@@ //

	//データ(double型)の読み込み
	public static final double loadDouble(String recordname, double defaultValue) {  //-----★
		
		ByteArrayInputStream  in=null;
		DataInputStream       dis=null;
		double                value=defaultValue;  //-----★
		
		byte[] bData=loadBytes(recordname);
		if (bData==null) return defaultValue;
		
		try {
			//データの読み出し
			in=new ByteArrayInputStream(bData);
			dis=new DataInputStream(in);
			
			value=dis.readDouble(); //-----★
			
			dis.close();

		} catch (Exception e) {
			System.out.println("MyRecord.loadDouble::"+e.toString());  //-----★
			if (dis!=null) {
				try { dis.close(); dis=null; in=null; } catch (Exception e2) {}
			}
			if (in!=null) {
				try { in.close(); in=null; } catch (Exception e2) {}
			}
			return defaultValue;
		}
		return value;
	}
//*/

// @@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@ //
// @@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@ //
// @@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@ //

} //MyRecordの終わり

// @@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@ //
