// @@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@ //
// Form1.java  フォーム
//
// (C)Tomarky   200*.01.01-
// @@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@ //

// インポート
import javax.microedition.lcdui.*;
//import javax.microedition.lcdui.game.*;
import javax.microedition.midlet.*;
//import javax.microedition.rms.*;
//import javax.microedition.media.*;
//import java.io.*;
//import java.util.*;


// @@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@ //

//
public class Form1 extends Form implements CommandListener, ItemStateListener, ItemCommandListener {
	private final String   RS_PARPRICE="tm.oapchecker.parprice";
	private MIDlet         midlet=null;
	private Command[]      command=new Command[2];
	private StringItem[]   stringitem=new StringItem[2];
	private TextField      textfield=null;
	private long           currentpacket=0L;

	//コンストラクタ
	Form1(MIDlet m) {
		super("OAP通信量の目安");
		
		midlet=m;
		
		long maxsize=0L, currentsize=0L;
		try {
			maxsize=Long.parseLong(System.getProperty("com.kddi.vm.netuse.max"));
			currentsize=Long.parseLong(System.getProperty("com.kddi.vm.netuse.current"));
		} catch(Exception e) {
		}
		double packetsize=(double)currentsize;
		packetsize=Math.ceil(packetsize/128.0);
		currentpacket=(long)packetsize;
		
		String maxsize2, currentsize2;
		maxsize2=unitup(maxsize);
		currentsize2=unitup(currentsize);
		
		Font titlefont=Font.getFont(Font.FACE_SYSTEM,Font.STYLE_UNDERLINED,Font.SIZE_MEDIUM);
		StringItem subtitle=null;
		
		subtitle=new StringItem(null,"本日の通信量\n");
		subtitle.setFont(titlefont);
		
		append(subtitle);
		append("　"+Long.toString(currentsize)+" ﾊﾞｲﾄ"
				+(currentsize>1024L?" ("+currentsize2+")":"")+"\n");
		append("　(約 "+Long.toString(currentpacket)+" ﾊﾟｹｯﾄ)\n");
		//append("　\n");
		append(new Spacer(240,5));

		subtitle=new StringItem(null,"一日に利用できる通信量\n");
		subtitle.setFont(titlefont);

		append(subtitle);
		append("　"+Long.toString(maxsize)+" ﾊﾞｲﾄ"
				+(maxsize>1024L?" ("+maxsize2+")":"")+"\n");
		double dblnum=(double)maxsize;
		append("　(約 "+Long.toString((long)Math.ceil(dblnum/128.0))+" ﾊﾟｹｯﾄ)\n");
		append("あと "+Long.toString(maxsize-currentsize)+" ﾊﾞｲﾄ通信出来ます\n");
		//append("　\n");
		append(new Spacer(240,5));

		subtitle=new StringItem(null,"OAPの通信料金の目安\n");
		subtitle.setFont(titlefont);
	
		append(subtitle);
		append("基本ﾊﾟｹｯﾄ通信料(円/ﾊﾟｹｯﾄ)\n");
		
		double parprice=MyRecord.loadDouble(RS_PARPRICE,0.21);
		
		textfield=new TextField(null,Double.toString(parprice),6,TextField.DECIMAL);
		
		append(textfield);
		append(stringitem[0]=new StringItem(null,null));
		append(stringitem[1]=new StringItem(null,null));
		stringitem[1].setFont(Font.getFont(Font.FACE_SYSTEM,Font.STYLE_PLAIN,Font.SIZE_SMALL));
		
		setItemStateListener(this);
		
		command[0]=new Command("終了",Command.EXIT,0);
		command[1]=new Command("記憶",Command.SCREEN,1);
		
		textfield.addCommand(command[1]);
		textfield.setItemCommandListener(this);
		
		addCommand(command[0]);
		setCommandListener(this);
		
		update();
	}
	
	private String unitup(long value) {
		double dblnum=(double)value;
		String unit="B";
		if (value>1024L) {
			if (value>(1024L*1024L)) {
				dblnum/=1024.0*1024.0;
				unit="MB";
			} else {
				dblnum/=1024.0;
				unit="KB";
			}
			if (dblnum<10.0)
				dblnum=Math.ceil(dblnum*100.0)/100.0;
			else if (dblnum<100.0)
				dblnum=Math.ceil(dblnum*10.0)/10.0;
			else 
				dblnum=Math.ceil(dblnum);
		}
		return Double.toString(dblnum)+unit;
	}
	
	private void update() {
		double parprice=0.0, price=0.0;
		try {
			parprice=Double.parseDouble(textfield.getString());
		} catch (Exception e) {
		}
		price=Math.ceil(parprice*(double)currentpacket);
		stringitem[0].setText("本日のOAP通信料金 約 "+Long.toString((long)price)+" 円\n");
	}
	
	//CommandListener
	public void commandAction(Command c, Displayable d) {
		if (c==command[0]) {
			midlet.notifyDestroyed();
		}
	}
	
	//ItemCommandListener
	public void commandAction(Command c, Item item) {
		if (c==command[1]) {
			double parprice=0.0;
			try {
				parprice=Double.parseDouble(textfield.getString());
			} catch (Exception e) {
			}
			MyRecord.saveDouble(RS_PARPRICE,parprice);
			stringitem[1].setText("基本ﾊﾟｹｯﾄ通信料 "+Double.toString(parprice)+" を記憶しました");
		}
	}
	
	//ItemStateListener
	public void itemStateChanged(Item item) {
		if (item==textfield) {
			update();
		}
	}
	

// @@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@ //

} //Form1の終わり

// @@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@ //
